import Adafruit_DHT
import time
from datetime import datetime
import sendEmail
from elasticsearch import Elasticsearch
from decimal import Decimal
import pytz
import requests


def getWeather():
    data = {}
    api_key = "71bbdbae42aa7f3bf8ae9d10daa0e805"
    base_url = "http://api.openweathermap.org/data/2.5/weather?"
    city_name = "Żyrardów"
    complete_url = base_url + "appid=" + api_key + "&q=" + city_name+"&units=metric&lang=pl"
    response = None
    try:
        response = requests.get(complete_url)
    except Exception as e:
        print("weather problem "+str(e))
        return data
    x = response.json()
    print(x)
    if x["cod"] != "404":

        y = x["main"]
        data["current_temperature"] = y["temp"]
        data["current_pressure"] = y["pressure"]
        data["current_humidiy"] = y["humidity"]
        data["current_feels_like"] = y["feels_like"]

        data["visibility"] = x["visibility"]
        data["windFrom"] = x["wind"]["deg"]
        data["windSpeed"] = x["wind"]["speed"]
        z = x["weather"]


        data["weather_description"] = z[0]["description"]
        print(data)
        #print(current_temperature,current_pressure,current_humidiy,current_feels_like)
        #print(visibility,windFrom,windSpeed)

    else:
        print(" City Not Found ")

    return data

def sendToElastic(doc):
    print("proba wyslanie do elastica")
    es = Elasticsearch('http://michaoelast.francecentral.cloudapp.azure.com',
        http_auth=('elastic','Wzyr045Wzyr045!'),timeout=30)
    #tz = pytz.timezone('Europe/Warsaw')
    #res = es.index("temp-"+datetime.now(tz=tz).strftime('%Y%m%d'),body=doc)
    idxName= "temp-"+datetime.now().strftime('%Y%m%d')
    res = es.index(idxName,body=doc)
    es.indices.refresh(index=idxName)
    print("SEND TO ELASTIC")



DHT_SENSOR22 = Adafruit_DHT.DHT22
DHT_SENSOR11 = Adafruit_DHT.DHT11
DHT_PIN22=4
DHT_PIN11=17
lashhum=None
lastTemp = None
lastHum2 = None
lastTemp2 = 0
lastTime = None
#es = Elasticsearch('http://michaoelastic.southcentralus.cloudapp.azure.com',
#        http_auth=('elastic','Wzyr045Wzyr045!'))
while True:
  try:
    hum,temp = Adafruit_DHT.read(DHT_SENSOR11,DHT_PIN11)
    hum2,temp2 = Adafruit_DHT.read(DHT_SENSOR22,DHT_PIN22)
    
    if hum is not None and temp is not None and hum2 and temp2:
        now = datetime.now()
        tz = pytz.timezone('Europe/Warsaw')
        elasticTime = datetime.now(tz=tz)
        print("/home/pi/dht/%s_temp.csv"%(now.strftime("%Y%m%d")))
        f = open("/home/pi/dht/%s_temp.csv"%(now.strftime("%Y%m%d")),"a")
        text = ("%s;%s;%s;%s;%s"%(now.strftime("%Y%m%d%H%M%S"),round(temp,3),round(hum,3),round(temp2,3),round(hum2,3)))
        print(text)
        f.write(text+"\n")
        f.close()
        msg=""
        if temp2 >= 20 and lastTemp2 < 20:
            msg = "Rozpoczynam grzanie"
            sendEmail.sendEmail(msg)

        elif temp2 <= 30 and lastTemp2 > 30:
            msg = "Zakonczylem grzanie"
            sendEmail.sendEmail(msg)

        else:
            msg = "Inny przypad t2 %s lt2 %s"%(str(temp2),str(lastTemp2))
        print("msg: %s"%(msg)) 

        lastTemp2 = temp2 
        if lastTime == None or lastTime.hour != now.hour:
            msg = ("%s;%s;%s"%(now.strftime("%Y%m%d%H%M%S"),round(temp,3),round(temp2,3)))
            sendEmail.sendEmail(msg,"RAPORT GODZINOWY")
            lastTime = datetime.now()
        weat = getWeather()
        doc = {
                'czas' : elasticTime,
                'temp1': temp,
                'hum1': hum,
                'temp2': temp2,
                'hum2': hum2
              }
        doc.update(weat)
        try:
           sendToElastic(doc)
        except Exception as e:
            print(str(e))
            try:
                time.sleep(5)
                sendToElastic(doc)
            except Exception as e:
                try:
                    time.sleep(5)
                    sendToElastic(doc)
                except Exception as e:
                    sendEmail.sendEmail("Cos nie tak z wys Elas "+str(e),"ERROR ELASTIC")

        time.sleep(60)
    else:
        print("BRAK")
  except Exception as e:
    print(str(e))
    pass

