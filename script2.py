import Adafruit_DHT
import time
from datetime import datetime

DHT_SENSOR22 = Adafruit_DHT.DHT22
DHT_SENSOR11 = Adafruit_DHT.DHT11
DHT_PIN22=4
DHT_PIN11=17

while True:
    hum,temp = Adafruit_DHT.read(DHT_SENSOR11,DHT_PIN11)
    hum2,temp2 = Adafruit_DHT.read(DHT_SENSOR22,DHT_PIN22)
    
    if hum is not None and temp is not None and hum2 and temp2:
        now = datetime.now()
        f = open("/home/pi/dht/%s_temp.csv"%(now.strftime("%Y%m%d")),"a")
        text = ("%s;%s;%s;%s;%s"%(now.strftime("%Y%m%d%H%M%S"),temp,hum,temp2,hum2))
        print(text)
        f.write(text+"\n")
        f.close()

        time.sleep(10)
    else:
        print("BRAK")
    

